#!/bin/bash
# ci-builder.sh is the interface between various CI systems and git-buildpackage

# To do this, it creates a gbp.conf specifically for this build to capture all
# resultant files in a temp directory.
# Then, based on which branch is being built, determines which distribution to
# build against (sitemaker for master, sitemaker-unstable for all other branches)
# For non-master builds, we create a changelog entry for a snapshot build.
# This version number uses the time of the build to ensure that snapshot version
# numbers are always incrementing, as apt won't upgrade an existing version if
# the version replacing it is considered older.
# After building, it collects up the artifacts into the OUTPUT directory for use
# by the CI system.
# Also, for master and develop, it adds the package to the sitemaker or
# sitemaker-unstable distributions respectively.

set -x
set -e

BUILD_NUMBER="$1"
TARGET_DIR="$(pwd)/OUTPUT"
TOOLS_DIR="$(pwd)/build"
REPO_DIR="/home/bamboo/repo"
GIT_BRANCH=$(git symbolic-ref --short HEAD)

# doCleanup will always run, even if any step in this script fails
function doCleanup {
	# Keep track of the last result - if we got here through
	# error we may not want do do things.
	ORIGINAL_ERROR=$?

	# No matter what, keep the test results
	COVERAGE_DIR="${BUILD_DIR}/tests"
	if [ -e "${COVERAGE_DIR}" ]; then
		cp -r ${COVERAGE_DIR} ${TARGET_DIR}/
	fi

	# Remove the tmpdir, but only if the build worked
	# We may want to look in to the directory to figure out what went wrong
	if [ ${ORIGINAL_ERROR} == 0 ]; then
		rm -r ${BUILD_DIR}
	fi
}
trap doCleanup EXIT

# Make sure the target directory exists and is empty
if [ -e ${TARGET_DIR} ]; then
	rm -r ${TARGET_DIR}
fi
mkdir ${TARGET_DIR}

BUILD_DIR=$(mktemp -d /tmp/build.XXXXX)

# Write out the git-buildpackage config for this package
cat >.gbp.conf <<EOF
[DEFAULT]
builder = ${TOOLS_DIR}/core-builder.sh
#pristine-tar = True
export = WC
export-dir = ${BUILD_DIR}
ignore-new = True
EOF

# TODO(nickh): Consider how we handle branches.
# For now only add to repo if on master or develop
# By default, build things against sitemaker-unstable,
BUILD_DIST="sitemaker-unstable"
case ${GIT_BRANCH} in
master)
	TARGET_DIST="sitemaker"
	BUILD_DIST="trusty"
	;;
develop)
	TARGET_DIST="sitemaker-unstable"
	;;
esac

# Update the changelog for a snapshot build, but only if on a non-master branch.
# `master` is special and should be bumped by hand in a thing that gets
# commited, not as part of of some build process that never goes anywhere.
if [ "${GIT_BRANCH}" != 'master' ]; then
	git-dch --debian-branch="${GIT_BRANCH}" --snapshot --snapshot-number="$(date +%s)" --auto
fi



# And build the thing (echo such that sbuild thinks it has an acceptable TTY) 
echo '' | git-buildpackage --dist=${BUILD_DIST}

# Grab the artefacts from the tmpdir and put them somewhere useful
cp ${BUILD_DIR}/*.{deb,changes,dsc,tar.gz} ${TARGET_DIR}/


if [ -n "${TARGET_DIST}" ]; then
	REPREPRO_CMD="reprepro -b ${REPO_DIR} -V -V -V --ignore=wrongdistribution"
	# Add the source package, then the binary packages
	${REPREPRO_CMD} includedsc ${TARGET_DIST} ${TARGET_DIR}/*.dsc &&
		${REPREPRO_CMD} include ${TARGET_DIST} ${TARGET_DIR}/*.changes
fi

# Run integration tests

# set -eu

# TOPLEVEL="$(dirname "$(dirname "$0")")"

# cd "$TOPLEVEL"

# function collect_test_results() {
#     mkdir -p OUTPUT/test/functional
#     [ ! -d test/functional/results/ ] || cp -r test/functional/results/ OUTPUT/test/functional/
# }

# trap collect_test_results EXIT

# # Build
# # Start server backgrounded

# test/functional/run-functional-tests
