#!/bin/bash
# core-builder.sh is a git-buildpackage builder that makes the things needed for sbuild.

# It makes a debian source description (dsc) file that describes the build
# and the tarballs it refers to.
# It does this rather than just running sbuild as without a dsc, it runs the
# clean step outside of the chroot.

set -x
set -e

# Find which package we're building
SOURCE_PKG=$(dpkg-parsechangelog --show-field "Source")
SOURCE_VERSION=$(dpkg-parsechangelog --show-field "Version")
COVERAGE_DIR="/tmp/coverage/${SOURCE_PKG}_${SOURCE_VERSION}"

function doCleanup {
	# Collect any test output into this build, no matter what
	if [ -e ${COVERAGE_DIR} ]; then
		cp -r "${COVERAGE_DIR}" ./tests
	fi
}

# Always do this, even if any step fails
# (sbuild will die if unit tests fail, but we still want the results)
trap doCleanup EXIT

# Build a DSC for the package
dpkg-source -b .

# Go out to the building directory which contains the .dsc (and soon deb and changes files)
cd ../


# Build the dsc with whatever arguments we were given inside a chroot.
sbuild "$@" -v -A "./${SOURCE_PKG}_${SOURCE_VERSION}.dsc"
