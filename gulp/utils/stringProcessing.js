var _ = require('lodash');


// Process Underscore templates in variables

function useUnderscoreTmpl(input, data) {

    var render = _.partialRight(_.template, data);

    var transform = function (item) {
        if (_.isString(item)) {
            return render(item);
        } else if (_.isArray(item)) {
            return _.map(item, transform);
        } else if (_.isPlainObject(item)) {
            return _.reduce(item, function (obj, value, key) {
                obj[render(key)] = transform(value);
                return obj;
            }, {});
        } else {
            return item;
        }
    };

    return transform(input);
}

module.exports = {
    underscore: useUnderscoreTmpl,
    underscorePreFilled: function (data) {
        return _.partialRight(useUnderscoreTmpl, data);
    }
};
