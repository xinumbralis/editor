var gulp = require('gulp');
var path = require('path');
var _ = require('lodash');

var vars = require('../variables.json');
var u = require('./stringProcessing').underscorePreFilled(vars);

var less = require('gulp-less');
var lessBasePrefix = _.compose(u, function () {
    return path.join(path.resolve(path.join(__dirname, '../../')), _.first(arguments));
});

module.exports = function () {

    return less({
        paths: _.map([
            '<%= paths.bower %>',
            '<%= paths.src %>/<%= paths.styles %>'
        ], lessBasePrefix)
    });
};
