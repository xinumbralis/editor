var pkg = require('../../package.json');
var vars = require('../variables.json');
var _ = require('lodash');

var header = require('gulp-header');

module.exports = function () {

    return header(
        vars.banner,
        _.extend({year: new Date().getFullYear()}, pkg)
    );
};
