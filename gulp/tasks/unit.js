var gulp = require('gulp');
var _ = require('lodash');

var vars = require('../variables.json');
var u = require('../utils/stringProcessing').underscorePreFilled(vars);

var karma = require('gulp-karma');

// Extract variables from karma config

var karmaConf = {
    set: function (conf) {
        this.get = conf;
    }
};
require(u('../../<%= paths.test %>/karma.conf.js'))(karmaConf);

// Remove jshint from preprocessors

var preprocessors = _.reduce(karmaConf.get.preprocessors, function (result, value, key) {
    var match = _.indexOf(value, 'jshint');
    if (~match) value.splice(match, 1);
    result[key] = value;
    return result;
}, {});


// Gulp task

module.exports = function () {

    return gulp.src(karmaConf.get.files)
        .pipe(karma({
            configFile: 'test/karma.conf.js',
            preprocessors: preprocessors,
            action: 'run'
        }));
};
