var gulp = require('gulp');
var path = require('path');
var _ = require('lodash');

var vars = require('../variables.json');
var u = require('../utils/stringProcessing').underscorePreFilled(vars);

var less = require('../utils/less');

module.exports = function () {

    return gulp.src(u('<%= paths.src %>/<%= paths.styles %>/*.less'))
        .pipe(less())
        .pipe(gulp.dest(u('<%= paths.build %>/<%= paths.styles %>')));
};
