var gulp = require('gulp');
var _ = require('lodash');

var pkg = require('../../package.json');
var vars = require('../variables.json');
var u = require('../utils/stringProcessing').underscorePreFilled(vars);

var uglify = require('gulp-uglify');
var banner = require('../utils/banner');


module.exports = function () {

    return gulp.src(u('<%= paths.build %>/**/*.js'), {cache: false})
        .pipe(uglify({
            mangle: true,
            preserveComments: false
        }))
        .pipe(banner())
        .pipe(gulp.dest(u('<%= paths.build %>')));
};
