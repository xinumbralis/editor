var fs = require('fs');
var gulp = require('gulp');
var path = require('path');
var merge = require('merge-stream');
var glob = require('glob');
var _ = require('lodash');

var vars = require('../variables.json');
var u = require('../utils/stringProcessing').underscorePreFilled(vars);

var concat = require('gulp-concat');


module.exports = function () {

    var pattern = path.resolve(path.join(__dirname, u('../../<%= paths.src %>/**/<%= files.externals %>'))),
        streams = [];

    _.each(glob.sync(pattern), function (file) {

        var externalsFile = fs.readFileSync(file),
            externals = JSON.parse(externalsFile);

        if (_.has(externals, 'copy')) {
            _.each(u(externals.copy), function (item) {
                streams.push(
                    gulp.src(item.src)
                        .pipe(gulp.dest(item.dest))
                );
            });
        }

        if (_.has(externals, 'concat')) {
            _.each(u(externals.concat), function (item) {
                streams.push(
                    gulp.src(item.src)
                        .pipe(concat(path.basename(item.dest)))
                        .pipe(gulp.dest(path.dirname(item.dest)))
                );
            });
        }
    });

    return merge.apply(null, streams);
};
