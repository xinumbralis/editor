var gulp = require('gulp');

var vars = require('../variables.json');
var u = require('../utils/stringProcessing').underscorePreFilled(vars);

var imagemin = require('gulp-imagemin');


module.exports = function () {

    return gulp.src(u('<%= paths.build %>/**/*.{png,jpg,gif}'))
        .pipe(imagemin({
            optimizationLevel: 7,
            pngquant: true
        }))
        .pipe(gulp.dest(u('<%= paths.build %>')));
};
