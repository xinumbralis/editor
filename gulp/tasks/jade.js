var gulp = require('gulp');
var pkg = require('../../package.json');

var vars = require('../variables.json');
var u = require('../utils/stringProcessing').underscorePreFilled(vars);

var jade = require('gulp-jade');

module.exports = function () {

    return gulp.src(u([
            '<%= paths.src %>/**/*.jade',
            '!<%= paths.src %>/<%= paths.app %>/**/*.jade'
        ]))
        .pipe(jade({
            locals: {
                pkg: pkg,
                angular: vars.angular
            },
            pretty: true
        }))
        .pipe(gulp.dest(u('<%= paths.build %>')));
};
