var gulp = require('gulp');
var argv = require('yargs').argv;
var connect = require('gulp-connect');

var gulpif = require('gulp-if');

module.exports = function () {

    // Connect reload has to be passed to the pipe - invoking scr with no files
    return gulp.src('!').pipe(gulpif(!!argv.livereload, connect.reload()));
};
