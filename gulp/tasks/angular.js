var gulp = require('gulp');
var path = require('path');
var merge = require('merge-stream');
var queue = require('streamqueue');
var _ = require('lodash');

var vars = require('../variables.json');
var u = require('../utils/stringProcessing').underscorePreFilled(vars);

var filesort = require('gulp-angular-filesort');
var annotate = require('gulp-ng-annotate');
var concat = require('gulp-concat');
var jade = require('gulp-jade');
var htmlmin = require('gulp-htmlmin');
var less = require('../utils/less');
var cssmin = require('gulp-cssmin');
var templateCache = require('gulp-angular-templatecache');
var styleInjector = require('../plugins/gulp-angular-style-injector');


module.exports = function () {

    return merge(

        gulp.src(u('<%= paths.src %>/<%= paths.app %>/**/*.js'))
            .pipe(filesort())
            .pipe(annotate({
                single_quotes: true
            }))
            .pipe(concat(u('<%= angular.files.main %>')))
            .pipe(gulp.dest(u('<%= paths.build %>/<%= paths.scripts %>'))),

        queue({objectMode: true},

              gulp.src(u([
                  '<%= paths.src %>/<%= paths.app %>/**/*.jade'
              ]))
                .pipe(jade())
                .pipe(htmlmin({
                    removeComments: true,
                    collapseWhitespace: true
                }))
                .pipe(templateCache('templates.js', {
                    root: u('/<%= paths.app %>/'),
                    module: u('<%= angular.mainModule %>')
                })),

            gulp.src(u('<%= paths.src %>/<%= paths.app %>/**/*.less'))
                .pipe(less())
                .pipe(cssmin({
                    keepSpecialComments: false
                }))
                .pipe(styleInjector('styles.js', {
                    module: u('<%= angular.mainModule %>')
                }))
        )
            .pipe(concat(u('<%= angular.files.resources %>')))
            .pipe(gulp.dest(u('<%= paths.build %>/<%= paths.scripts %>')))
    )
};
