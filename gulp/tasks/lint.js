var gulp = require('gulp');

var vars = require('../variables.json');
var u = require('../utils/stringProcessing').underscorePreFilled(vars);

var jshint = require('gulp-jshint');
var stylish = require('jshint-stylish');

module.exports = function () {

    return gulp.src(u('<%= paths.src %>/<%= paths.app %>/**/*.js'))
        .pipe(jshint())
        .pipe(jshint.reporter(stylish))
        .pipe(jshint.reporter('fail'))
};
