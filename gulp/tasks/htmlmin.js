var gulp = require('gulp');

var vars = require('../variables.json');
var u = require('../utils/stringProcessing').underscorePreFilled(vars);

var htmlmin = require('gulp-htmlmin');


module.exports = function () {

    return gulp.src(u('<%= paths.build %>/**/*.html'))
        .pipe(htmlmin({
            removeComments: true,
            collapseWhitespace: true
        }))
        .pipe(gulp.dest(u('<%= paths.build %>')));
};
