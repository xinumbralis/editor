var gulp = require('gulp');

var vars = require('../variables.json');
var u = require('../utils/stringProcessing').underscorePreFilled(vars);


module.exports = function () {

    return gulp.src(u([
            '<%= paths.src %>/**/*.*',
            '!<%= paths.src %>/**/*.less',
            '!<%= paths.src %>/**/*.jade',
            '!<%= paths.src %>/**/<%= files.externals %>',
            '!<%= paths.src %>/<%= paths.app %>/**/*'
        ]))
        .pipe(gulp.dest(u('<%= paths.build %>')));
};
