var gulp = require('gulp');

var vars = require('../variables.json');
var u = require('../utils/stringProcessing').underscorePreFilled(vars);

var cssmin = require('gulp-cssmin');
var banner = require('../utils/banner');


module.exports = function () {

    return gulp.src(u('<%= paths.build %>/**/*.css'))
        .pipe(cssmin({
            keepSpecialComments: 0
        }))
        .pipe(banner())
        .pipe(gulp.dest(u('<%= paths.build %>')));
};
