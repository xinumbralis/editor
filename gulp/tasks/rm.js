var gulp = require('gulp');

var vars = require('../variables.json');
var u = require('../utils/stringProcessing').underscorePreFilled(vars);

var rimraf = require('gulp-rimraf');


module.exports = function () {

    return gulp.src(u([
            '<%= paths.build %>'
        ]))
        .pipe(rimraf({force: true}));
};
