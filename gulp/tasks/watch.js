var gulp = require('gulp');

var vars = require('../variables.json');
var u = require('../utils/stringProcessing').underscorePreFilled(vars);


module.exports = function () {

    return gulp.watch(u(['<%= paths.src %>/**/*']), ['triggerReload']);
};
