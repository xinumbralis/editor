var gulp = require('gulp');
var argv = require('yargs').argv;
var _ = require('lodash');

var vars = require('../variables.json');
var u = require('../utils/stringProcessing').underscorePreFilled(vars);

var connect = require('gulp-connect');
var serverPort = (argv.port && _.isNumber(argv.port) && argv.port > 0) ? argv.port : vars.server.port;


module.exports = function () {

    connect.server({
        root: u('<%= paths.build %>'),
        port: serverPort,
        livereload: !!argv.livereload
    });
};
