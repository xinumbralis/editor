var gulp = require('gulp');
var _ = require('lodash');

var NO_DEPS = ':noDeps';


// Load list of requested Gulp tasks

module.exports = function (tasks) {

    _.each(tasks, function (item) {

        var task, deps;

        if (_.isString(item)) {

            // Load basic task
            gulp.task(item, require('./tasks/' + item));

        } else if (_.isPlainObject(item)) {

            if (_.isString(item.name) && !_.isEmpty(item.name)) {

                // Custom name for task
                task = _.isString(item.task) ? item.task : item.name;

                // Set deps
                deps = _.isArray(item.deps) ? item.deps : [];

                // Load tasks with deps
                gulp.task(item.name, deps, require('./tasks/' + task));

                if (deps.length) {

                    // If task has deps - create no deps version
                    gulp.task(item.name + NO_DEPS, require('./tasks/' + task));
                }
            }
        }
    });

    return gulp;
};
