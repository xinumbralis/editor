// Load Gulp tasks

var gulp = require('./gulp')([

    // Low level tasks
    'rm',
    'lint',
    'unit',

    // Dev tasks
    {name: 'copy', deps: ['rm']},
    {name: 'externals', deps: ['rm']},
    {name: 'less', deps: ['rm']},
    {name: 'jade', deps: ['rm']},
    {name: 'angular', deps: ['rm']},

    // Build tasks
    {name: 'htmlmin', deps: ['dev']},
    {name: 'cssmin', deps: ['dev']},
    {name: 'uglify', deps: ['dev']},
    {name: 'images', deps: ['dev']},

    // Dev utils
    {name: 'serve', deps: ['dev']},
    {name: 'triggerReload', deps: ['dev']},
    {name: 'watch', deps: ['dev']}
]);


// Task aliases

gulp.task('dev', [
    'copy',
    'externals',
    'less',
    'jade',
    'angular'
]);

gulp.task('build', [
    'htmlmin',
    'cssmin',
    'uglify',
    'images'
]);

gulp.task('test', [
    'lint',
    'unit'
]);

gulp.task('start', [
    'serve',
    'watch'
]);

gulp.task('default', ['start']);
