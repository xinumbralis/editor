# coding=utf-8

import argparse
import os
import sys
import time
import unittest

from selenium import webdriver
from xmlrunner import XMLTestRunner


# Css Selectors for Buttons
LOGIN_BUTTON = "a.header_button.login_button"
SIGNIN_BUTTON = "form#login-form .login"
LOGOUT_BUTTON = "a#menu_option_6"
CONTINUE_COOKIE_BUTTON = "a.accept"

# Css selectors for Textboxs
USERNAME_TEXTBOX = "input#login-username"
PASSWORD_TEXTBOX = "input#login-password"

# Css selectors for Text
USERNAME_DASHBOARD = "p.username"
INVALID_UIDPWD_ERRORMESSAGE = "span.validation-message"
INVALID_UID_ERRORMESSAGE = "span.validation-message"
SPCHAR_UID_ERRORMESSAGE = "span.validation-message"
PASSWORD_2CHAR_ERRORMESSAGE = "form#login-form .password-container .validation-message"
ACCOUNT_DASHBOARD = "div#header_items .button_title"

# Data to test moonfruit login
TEST_LOGIN_DATA = [{
    "username": "epuser1",
    "password": "password",
    "expected_data": "epuser1",
    "handler": "valid_userid"
},
    {
    "username": "epuser1",
    "password": "password1",
    "expected_data": "Username or password are invalid",
    "handler": "invalid_password"
},
    {
    "username": "ep",
    "password": "password",
    "expected_data": "Please enter a valid username.",
    "handler": "id_two_characters"
},
    {
    "username": u"£$abcdef",
    "password": "password",
    "expected_data": "Please enter a valid username.",
    "handler": "id_special_characters"
}]


class MFTest(unittest.TestCase):

    # Testcase to test cookies policy and login function with different data
    def __init__(
            self, selenium_hub_url, browser, browser_version, platform, platform_version, moonfruit_host, testcase):
        unittest.TestCase.__init__(self, testcase)
        self.selenium_hub_url = selenium_hub_url
        self.browser = browser
        self.browser_version = browser_version
        self.platform = platform
        self.platform_version = platform_version
        self.moonfruit_host = moonfruit_host

    # Setup will be executed before every test
    def setUp(self):
        self.driver = webdriver.Remote(command_executor=self.selenium_hub_url, desired_capabilities={
            "browserName": self.browser,
            "browserVersion": self.browser_version,
            "platform": self.platform,
            "platformVersion": self.platform_version,
        })
        self.driver.implicitly_wait(10)
        self.base_url = "http://{}/".format(self.moonfruit_host)
        self.verificationErrors = []
        self.imagedir = "results"

    # Verify cookies policy displayed and accept
    def test_cookie_policy_banner(self):
        driver = self.driver
        driver.get(self.base_url)
        driver.find_element_by_css_selector(CONTINUE_COOKIE_BUTTON).click()
        cookie_accepted = driver.get_cookie('cookie_acceptance')
        self.assertEquals(
            cookie_accepted['value'], u"1", "Failed to accept the cookies policy")

    # Verify login with different data
    def test_datadriven_login(self):

        handlers = Handlers(self.driver)
        driver = self.driver

        # Enter login details
        def test_login_user(username, password):
            driver.get(self.base_url)
            driver.find_element_by_css_selector(LOGIN_BUTTON).click()
            driver.find_element_by_css_selector(USERNAME_TEXTBOX).clear
            driver.find_element_by_css_selector(USERNAME_TEXTBOX).send_keys(username)
            driver.find_element_by_css_selector(PASSWORD_TEXTBOX).clear
            driver.find_element_by_css_selector(PASSWORD_TEXTBOX).send_keys(password)
            driver.find_element_by_css_selector(SIGNIN_BUTTON).click()
            time.sleep(10)

        # Test login using handlers
        for test_case in TEST_LOGIN_DATA:
            username = test_case['username']
            password = test_case['password']
            expected_data = test_case['expected_data']
            handler = test_case['handler']
            test_login_user(username, password)
            handlers.dispatch(test_case['handler'], test_case)

    # Teardown will be executed after every test
    def tearDown(self):
        self.driver.implicitly_wait(30)
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)


class Handlers(MFTest):

    def __init__(self, driver):
        self.driver = driver

    # Handler to verify login with valid data
    def valid_userid(self, logindata):
        driver = self.driver
        username_dashboard = driver.find_element_by_css_selector(USERNAME_DASHBOARD)
        self.assertEqual(username_dashboard.text,
                         logindata['expected_data'],
                         "Failed to find correct username on dashboard")
        if driver.find_element_by_css_selector(ACCOUNT_DASHBOARD).is_displayed():
            driver.find_element_by_css_selector(ACCOUNT_DASHBOARD).click()
            driver.find_element_by_css_selector(LOGOUT_BUTTON).click()
            time.sleep(5)

    # Handler to verify login error message for invalid password
    def invalid_password(self, logindata):
        driver = self.driver
        error_message = driver.find_element_by_css_selector(INVALID_UIDPWD_ERRORMESSAGE).text
        self.assertEqual(error_message,
                         logindata['expected_data'],
                         "Failed to display correct error message when invalid password is entered")

    # Handler to verify login error message for id with two characters
    def id_two_characters(self, logindata):
        driver = self.driver
        error_message = driver.find_element_by_css_selector(INVALID_UID_ERRORMESSAGE).text
        self.assertEquals(error_message,
                          logindata['expected_data'],
                          "Failed to display correct error message when only two characters entered in user_id field")

    # Handler to verify login error message for id with special characters
    def id_special_characters(self, logindata):
        driver = self.driver
        error_message = driver.find_element_by_css_selector(SPCHAR_UID_ERRORMESSAGE).text
        self.assertEquals(error_message,
                          logindata['expected_data'],
                          "Failed to display correct error message when special characters entered in user_id field")

    # dispacher for handlers
    def dispatch(self, handler_name, args):
        func = getattr(self, handler_name)
        func(args)

# Unittest main methon to run the script
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Selenium test case for Moonfruit.com')
    parser.add_argument('selenium_hub_url', type=str, help='The url for selenium hub')
    parser.add_argument('browser', type=str, help='The type of the browser to execute the scripts')
    parser.add_argument('browser_version', type=int, help='The version of the browser')
    parser.add_argument('platform', type=str, help='The OS of the node machine')
    parser.add_argument('platform_version', type=int, help='The OS version of the node machine')
    parser.add_argument('moonfruit_host', type=str, help='Moonfruit site url')
    args = parser.parse_args()

    # Make results folder if dosen't exist
    if not os.path.exists('results'):
        os.makedirs('results')

    # Create a unittest suite
    suite = unittest.TestSuite()
    test_names = ['test_cookie_policy_banner', 'test_datadriven_login']
    for testname in test_names:
        suite.addTest(MFTest(
            args.selenium_hub_url,
            args.browser,
            args.browser_version,
            args.platform,
            args.platform_version,
            args.moonfruit_host,
            testcase=testname
        ))

    # Run the suite and generate the xml results and store in results folder
    output_file = file('results/results_MFTest_{}{}_{}{}.xml'.format(
        args.platform, args.platform_version, args.browser, args.browser_version),
        "w")
    runner = XMLTestRunner(output_file)
    runner.run(suite)
    # unittest.TextTestRunner(verbosity=2).run(suite)

