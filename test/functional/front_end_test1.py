# coding=utf-8

import argparse
import os
import sys
import time
import unittest

from selenium import webdriver
from selenium.webdriver.common.by import By
from xmlrunner import XMLTestRunner


# Css selectors for Front end page
# Css selectors for Text
PAGE_HEADING = "div.text-center>h1"

# Css selectors for Images
MOONFRUIT_LOGO = "div.text-center img"

# Css selectors for Buttons
ANGULAR_DIRECTIVE_BUTTON = "button.btn.btn-default"

# Css selectors for Text box
ANGULAR_DIRECTIVE_TEXTBOX = "input.form-control"


class Front_End_Test(unittest.TestCase):

    def __init__(
            self, selenium_hub_url, browser, browser_version, platform, platform_version, page_host, testcase):
        unittest.TestCase.__init__(self, testcase)
        self.selenium_hub_url = selenium_hub_url
        self.browser = browser
        self.browser_version = browser_version
        self.platform = platform
        self.platform_version = platform_version
        self.page_host = page_host

    def setUp(self):
        self.driver = webdriver.Remote(command_executor=self.selenium_hub_url, desired_capabilities={
            "browserName": self.browser,
            "browserVersion": self.browser_version,
            "platform": self.platform,
            "platformVersion": self.platform_version,
        })
        self.driver.implicitly_wait(10)
        self.base_url = "http://{}".format(self.page_host)
        self.verificationErrors = []
        self.imagedir = "results"

    def test_front_end_page(self):
        driver = self.driver
        driver.get(self.base_url)

        # Verify the page title
        try:
            self.assertEqual(driver.title, "Index", "Wront title is displayed on Front-end page")
        except AssertionError as e:
            self.verificationErrors.append(str(e))

        # Verify the page heading
        page_heading = driver.find_element(By.CSS_SELECTOR, PAGE_HEADING).text
        try:
            self.assertEqual(page_heading, "Front-end Resources", "Failed to display the page heading")
        except AssertionError as e:
            self.verificationErrors.append(str(e))

        # Verify moonfruit logo on the page
        mf_logo = driver.find_element(By.CSS_SELECTOR, MOONFRUIT_LOGO)
        try:
            self.assertIn('/images/moonfruit-logo.png', mf_logo.get_attribute("src"), "Failed to load moonfruit logo")
        except AssertionError as e:
            self.verificationErrors.append(str(e))

        # Verify text box on the page
        text_on_test_angular_button = driver.find_element(By.CSS_SELECTOR, ANGULAR_DIRECTIVE_BUTTON).text
        try:
            self.assertEqual(text_on_test_angular_button, "Test Angular Directive",
                        "Failed to display correct text on angular directive button")
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        driver.find_element(By.CSS_SELECTOR, ANGULAR_DIRECTIVE_BUTTON).click()
        time.sleep(5)
        text_displayed_in_textbox = driver.find_element(By.CSS_SELECTOR,
                                                        ANGULAR_DIRECTIVE_TEXTBOX).get_attribute("value")
        try:
            self.assertEqual(text_displayed_in_textbox, "It works!",
                        "Failed to display the text when clicked on angular directive button")
        except AssertionError as e:
            self.verificationErrors.append(str(e))

     # Teardown will be executed after every test
    def tearDown(self):
        self.driver.implicitly_wait(30)
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

# Unittest main methon to run the script
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Validate the Front-end page')
    parser.add_argument('selenium_hub_url', type=str, help='The url for selenium hub')
    parser.add_argument('browser', type=str, help='The type of the browser to execute the scripts')
    parser.add_argument('browser_version', type=int, help='The version of the browser')
    parser.add_argument('platform', type=str, help='The OS of the node machine')
    parser.add_argument('platform_version', type=int, help='The OS version of the node machine')
    parser.add_argument('page_host', type=str, help='Page url')
    args = parser.parse_args()

    # Make results folder if dosen't exist
    if not os.path.exists('results'):
        os.makedirs('results')

    # Create a unittest suite
    suite = unittest.TestSuite()
    test_names = ['test_front_end_page']
    for testname in test_names:
        suite.addTest(Front_End_Test(
            args.selenium_hub_url,
            args.browser,
            args.browser_version,
            args.platform,
            args.platform_version,
            args.page_host,
            testcase=testname
        ))

    Run the suite and generate the xml results and store in results folder
    output_file = file('results/results_Front_End_Test_{}{}_{}{}.xml'.format(
        args.platform, args.platform_version, args.browser, args.browser_version),
        "w")
    runner = XMLTestRunner(output_file)
    runner.run(suite)
    #unittest.TextTestRunner(verbosity=2).run(suite)
