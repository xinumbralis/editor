var _ = require('lodash');
var path = require('path');

var vars = require('../gulp/variables.json');
var u = require('../gulp/utils/stringProcessing').underscorePreFilled(vars);
var scriptExternals = u(require('../src/scripts/externals.json').concat[0].src);

module.exports = function (config) {
    config.set({

        basePath: '../',

        preprocessors: u({
            '<%= paths.src %>/<%= paths.app %>/**/*.jade': ['ng-jade2js'],
            '<%= paths.src %>/<%= paths.app %>/**/*.js': ['jshint', 'coverage']
        }),

        files: _.flatten([
            scriptExternals,
            u([
                '<%= paths.bower %>/angular-mocks/angular-mocks.js',
                '<%= paths.src %>/<%= paths.app %>/**/*.js',
                '<%= paths.src %>/<%= paths.app %>/**/*.jade',
                '<%= paths.test %>/unit/**/*Spec.js'
            ])
        ]),

        autoWatch: true,

        singleRun: true,

        frameworks: ['jasmine'],

        browsers: [
//            'Chrome',
//            'Firefox',
            'PhantomJS'
        ],

        reporters: [
            'progress',
            'junit',
            'coverage'
        ],

        plugins: [
            'karma-chrome-launcher',
            'karma-coverage',
            'karma-firefox-launcher',
            'karma-jasmine',
            'karma-jshint-preprocessor',
            'karma-junit-reporter',
            'karma-ng-jade2js-preprocessor',
            'karma-phantomjs-launcher'
        ],

        ngJade2JsPreprocessor: {
            stripPrefix: u('<%= paths.src %>/'),
            prependPrefix: '/',
            templateExtension: 'html',
            moduleName: 'htmlTemplates'
        },

        junitReporter: {
            outputFile: u('<%= paths.testReport %>/unit.xml'),
            suite: 'unit'
        },

        coverageReporter: {
            type: 'html',
            dir: u('<%= paths.testCoverage %>/')
        }

    });
};
