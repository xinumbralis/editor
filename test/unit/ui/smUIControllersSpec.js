'use strict';

describe('smUI.controllers', function() {

    beforeEach(module('smUI.controllers'));

    describe('UIPanelDesignCtrl', function() {

        var UIPanelDesignCtrl;

        beforeEach(inject(function ($controller, $rootScope) {
            UIPanelDesignCtrl = $rootScope.$new();
            $controller('UIPanelDesignCtrl as ctrl', {$scope: UIPanelDesignCtrl});
        }));

        it('Should return array.', function () {
            var isArray = _.isArray(UIPanelDesignCtrl.ctrl.times(5));
            expect(isArray).toBeTruthy();
        });

        it('Array should be of correct size.', function () {
            var n1 = UIPanelDesignCtrl.ctrl.times(1),
                n3 = UIPanelDesignCtrl.ctrl.times(3);
            expect(n1.length).toEqual(1);
            expect(n3.length).toEqual(3);
        });

        it('Array should contain unique values.', function () {
            var original = UIPanelDesignCtrl.ctrl.times(5),
                unique = _.unique(original);
            expect(original.length).toEqual(unique.length);
        });
    });

});
