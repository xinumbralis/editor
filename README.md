# Front-end Resources

Here are basic instructions for now. More detailed documentation to follow.



## Installation

Requirements:

* [Node.js](http://nodejs.org/)
* [NPM](https://www.npmjs.org/)
* [Gulp](http://gulpjs.com/)
* [Bower](http://bower.io/)
* [Karma](http://karma-runner.github.io/)

```
$ npm install -g gulp
$ npm install -g bower
$ npm install -g karma-cli
```

When your environment is ready:

```
$ npm install
```

This should install all the **NPM** dependencies and also automatically install **Bower** dependencies as well.



## Development

Source code is located in `src` directory.
Paths, file names and other settings are stored in `gulp/variables.json`.

Project uses **Gulp** task runner:

### Default task

```
$ gulp
// or
$ npm start
```

Runs default **Gulp** task which is an alias for `start` task.

### Start task

```
$ gulp start
$ gulp start --livereload
$ gulp start --port=8080
$ gulp start --livereload --port=8080
```

Runs `dev`, `serve`, and `watch` tasks.
You can pass `--livereload` flag to enable live reload on change during watch.
To change port for the static server specify the custom port as `--port={PORT}`

### Dev task

```
$ gulp dev
```

Build project for development to the `build_static` directory. No minification is performed.
The build directory can be changed in `gulp/variables.json`.

### Watch task

```
$ gulp watch
```

Watch will wait for changes in `src` directory and automatically trigger `dev` task.
If you have used `--livereload` flag and you have also started server at the same time 
the watch should trigger the live reload on change.

### Test task

```
$ gulp test
// or
$ npm test
```

Runs **JSHint** for files in `src` directory and test with **Jasmine**.
Tests will also save reports:

* **JUnit** to `test_out` folder
* **Istanbul** code coverage of the JavaScript files in `src/app` to `test_coverage` folder.

_Note: `npm test` command will automatically run "pre-test" which will execute `npm install`.
This means that if you want to just run the unit test you can just checkout the Git repo and run `npm test`._

### Server (dev) task

```
$ gulp serve
$ gulp serve --port=8080
```

Starts up **Connect** server and uses `build` directory as static root.
To change a port specify custom port in `--port={PORT}`. Default port is specified in `gulp/variables.json`.

### List of all tasks

```
$ gulp -T
```

Every task with dependencies will automatically receive `:noDeps` variant so you can issue just a single task.


## Build for production

```
$ gulp build
```

Build for production. Runs tests, builds and optimizes front-end resources into `build_static` directory.
The build directory can be changed in `gulp/variables.json`.
