// Main App
// Include all necessary modules for the editor app

angular.module('smMain', [
    'smCore',
    'smUI'
]);
