// App core functionality

angular.module('smCore', ['smCore.streams'])

    .config([
        'smAppBusProvider', 'smAppEventsProvider',
        function (appBusProvider, appEventsProvider) {

            appBusProvider.register(['messaging', 'transport']);

            appEventsProvider.register('keyStroke', [
                'libStreams', '$document',
                function (Streams, $document) {
                    return Streams.fromEventTarget($document, 'keyup');
                }
            ]);
        }
    ])

    .run([
        'smAppBus', 'smAppEvents',
        function (appBus, appEvents) {

            var m = appBus.get('messaging');
            var t = appBus.get('transport');

            m.onValue(function () {
                console.log('messaging bus', arguments);
            });

            m.plug(appEvents.get('keyStroke').map('.keyCode'));

            console.log('messaging bus instance', m);
            console.log('messaging bus is live', appBus.isLive('messaging'));
            console.log('transport bus instance', t);
            console.log('transport bus is live', appBus.isLive('transport'));
            console.log('transport bus send end trigger', appBus.kill('transport'));
            console.log('transport bus instance', t);
            console.log('transport bus is live', appBus.isLive('transport'));
            console.log('messaging bus is live', appBus.isLive('messaging'));

            console.log('undefined bus', appBus.get('undefined bus'));
            console.log('undefined bus is live', appBus.isLive('undefined bus'));
            console.log('undefined event', appEvents.get('undefined event'));
        }
    ]);
