// App core functionality

angular.module('smCore.streams', ['smLib.streams', 'smCore.error'])

    .provider('smAppBus', function appBusProvider() {

        var setupOnStart = [];

        this.register = function register(name) {
            setupOnStart = setupOnStart.concat(_.toArray(name));
        };

        this.$get = [
            'libStreams', 'smErrorFactory',
            function smAppBus(Streams, errorFactory) {

                var streamsError = errorFactory('smCore.streams');

                var deadBus = new Streams.Bus(),
                    appBus = {},
                    store = {};

                // Turn deadBus into inactive bus by ending the stream
                deadBus.end();

                function streamExists(name) {
                    return !_.isEmpty(store[name]) && !_.isEmpty(store[name].stream);
                }

                // Create stream with unsubscriber which will mark the bus as dead on Bus.end
                function createEndListener(name) {
                    return function () {
                        if (streamExists(name)) store[name].live = false;
                    };
                }

                function createBus(name) {
                    var newBus;
                    if (streamExists(name)) throw streamsError('1', 'Event bus "{0}" already exists.', name);
                    newBus = new Streams.Bus();
                    newBus.onEnd(createEndListener(name));
                    store[name] = {
                        stream: newBus,
                        live: true
                    };
                    return store[name].stream;
                }

                appBus.get = function get(name) {
                    return streamExists(name) ? store[name].stream : deadBus;
                };

                appBus.isLive = function isLive(name) {
                    return streamExists(name) && store[name].live;
                };

                appBus.kill = function kill(name) {
                    if (streamExists(name) && _.isFunction(store[name].stream.end)) {
                        store[name].stream.end();
                        return true;
                    }
                    return false;
                };

                // Register buses on startup
                _.each(setupOnStart, createBus);
                setupOnStart = [];

                return appBus;
            }
        ];
    })


    .provider('smAppEvents', function appBusProvider() {

        var setupOnStart = [];

        function pushNewEvent(name, event) {
            setupOnStart.push({
                name: name,
                event: event
            });
        }

        this.register = function register(item) {
            if (arguments.length === 1 && _.isPlainObject(item)) {
                _.each(item, function (value, key) {
                    pushNewEvent(key, value);
                });
            } else if (arguments.length > 1) {
                pushNewEvent.apply(null, arguments);
            }
        };

        this.$get = [
            'libStreams', 'smErrorFactory', '$injector',
            function smAppEvent(Streams, errorFactory, $injector) {

                var streamsError = errorFactory('smCore.streams');

                var deadEvent = Streams.never(),
                    appEvents = {},
                    store = {};

                function isEventStream(obj) {
                    return obj instanceof Streams.EventStream;
                }

                function eventExists(name) {
                    return !_.isEmpty(store[name]);
                }

                function createEvent(item) {

                    var newStream;

                    if (_.isUndefined(item.name) || eventExists((item.name))) {
                        throw streamsError('2', 'Event stream "{0}" already exists.', item.name);
                    }

                    newStream = $injector.invoke(item.event);

                    if (!isEventStream(newStream)) {
                        throw streamsError('3', 'Can\'t add "{0}". "{1}" is not an event stream.', item.name, newStream);
                    }

                    store[item.name] = newStream;
                    return store[item.name];
                }

                appEvents.get = function get(name) {
                    return eventExists(name) ? store[name] : deadEvent;
                };

                // Register events on startup
                _.each(setupOnStart, createEvent);
                setupOnStart = [];

                return appEvents;
            }
        ];
    });
