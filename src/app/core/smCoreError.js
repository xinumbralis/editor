// App core functionality

angular.module('smCore.error', [])

    /**
     * @description
     *
     * Use this factory across SM app to throw errors.
     *
     * var exampleError = errorFactory('example');
     * throw exampleError('CODE', 'This {0} is {1} wrong.', foo, bar);
     */

    .factory('smErrorFactory', function errorFactory() {

        return _.memoize(function customErrorFactory(module, CustomErrorConstructor) {

            var moduleName = _.isEmpty(module) ? false : '' + module,
                ErrorConstructor = _.isFunction(CustomErrorConstructor) ? CustomErrorConstructor : Error,

                stringify = function stringify(item) {

                    if (_.isFunction(item)) {
                        return item.toString().replace(/ \{[\s\S]*$/, '');
                    } else if (_.isUndefined(item)) {
                        return 'undefined';
                    } else if (!_.isString(item)) {
                        return JSON.stringify(item);
                    }

                    return item;
                };

            return function throwCustomError(/* args */) {

                var args = _.toArray(arguments),
                    code = args.shift(),
                    prefix = '[SM:' + (moduleName ? moduleName + ':' : '') + code + '] ',
                    tmpl = args.shift(),
                    tmplArgs = args,
                    message = prefix + tmpl.replace(/\{\d+\}/g, function (match) {
                        var index = _.parseInt(match.slice(1, -1));
                        if (index < tmplArgs.length) return stringify(tmplArgs[index]);
                        return match;
                    });

                return new ErrorConstructor(message);
            };
        });
    });
