// Directives for smUI module

angular
    .module('smUI.directives', [])

    .directive('smPanelAccordion', function () {
        return {
            restrict: 'A',
            link: function (scope, element) {

                element.click(function () {
                    element.toggleClass('open');
                    element.next().toggleClass('open');
                });
            }
        };
    });
