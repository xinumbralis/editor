// Mock data for UI

angular
    .module('smUI.mockData', [])

    .constant('UIModelDefault', {

        header: {

            siteMenu: [
                {
                    title: 'My other site name',
                    action: '#'
                },
                {
                    title: 'Second site name',
                    action: '#'
                },
                {
                    title: 'Third site name',
                    action: '#'
                },
                {
                    title: 'Other site in list name',
                    action: '#'
                },
                {
                    title: 'Last site in my account name',
                    action: '#'
                },
                {
                    title: 'Last site in my account name',
                    action: '#'
                },
                {
                    divider: true
                },
                {
                    title: 'Create a new site',
                    action: '#',
                    icon: 'fa-plus-circle'
                }
            ],

            deviceTools: [
                {
                    title: 'Mobile',
                    action: '#ui-mode-xs-active',
                    style: 'ui-btn-square-icon-bigger ui-main-icon-mobile',
                    icon: 'fa-mobile'
                },
                {
                    title: 'Tablet',
                    action: '#ui-mode-xs ui-mode-sm-active',
                    style: 'ui-btn-square-icon-medium ui-main-icon-tablet',
                    icon: 'fa-tablet'
                },
                {
                    title: 'Laptop',
                    action: '#ui-mode-xs ui-mode-sm ui-mode-md-active',
                    style: 'ui-btn-square-icon-bigger ui-main-icon-laptop',
                    icon: 'fa-laptop'
                },
                {
                    title: 'Desktop',
                    action: '#ui-mode-xs ui-mode-sm ui-mode-md ui-mode-lg-active',
                    style: 'ui-btn-square-icon-smaller ui-main-icon-desktop',
                    icon: 'fa-desktop'
                }
            ],

            pageTools: {
                button: {
                    title: 'Publish',
                    action: '#'
                },
                nav: [
                    {
                        title: 'Preview the site',
                        action: '#',
                        icon: 'fa-eye'
                    },
                    {
                        title: 'Site healthy check',
                        action: '#',
                        icon: 'fa-medkit'
                    },
                    {
                        title: 'Save as a draft',
                        action: '#',
                        icon: 'fa-file-o'
                    },
                    {
                        title: 'Make and share snapshot',
                        action: '#',
                        icon: 'fa-camera'
                    }
                ]
            },

            appMenu: [
                {
                    title: 'Notifications',
                    action: '#',
                    icon: 'fa-bell'
                },
                {
                    title: 'User account settings',
                    action: '#',
                    icon: 'fa-user'
                },
                {
                    title: 'Editor settings',
                    action: '#',
                    icon: 'fa-wrench'
                },
                {
                    title: 'Help',
                    action: '#',
                    icon: 'fa-question-circle'
                },
                {
                   divider: true
                },
                {
                    title: 'Log out',
                    action: '#',
                    icon: 'fa-sign-out'
                }
            ],

            toolbar: [
                {
                    tools: [
                        {
                            title: 'Sitemap',
                            type: 'link',
                            action: '#',
                            icon: 'fa-sitemap'
                        },
                        {
                            title: 'Design',
                            type: 'state',
                            action: '.design',
                            icon: 'fa-edit'
                        },
                        {
                            title: 'Themes',
                            type: 'state',
                            action: '.theme',
                            icon: 'fa-magic'
                        },
                        {
                            title: 'Dashboard',
                            type: 'link',
                            action: '#',
                            icon: 'fa-dashboard'
                        }
                    ]
                },
                {
                    tools: [
                        {
                            title: 'Assets',
                            type: 'state',
                            action: '.assets',
                            icon: 'fa-folder-open'
                        },
                        {
                            title: 'Upload',
                            type: 'link',
                            action: '#',
                            icon: 'fa-upload'
                        }
                    ]
                },
                {
                    last: true,
                    tools: [
                        {
                            title: 'Settings',
                            type: 'link',
                            action: '#',
                            icon: 'fa-cog'
                        }
                    ]
                }
            ]
        },

        panel: {

            design: [
                {
                    title: 'Tools',
                    tools: [
                        {
                            title: 'Text',
                            action: 'ADD_TEXT',
                            params: {
                                type: 'PARAGRAPH',
                                placeholder: true
                            },
                            icon: 'fa-font'
                        },
                        {
                            title: 'Heading',
                            action: 'ADD_TEXT',
                            params: {
                                type: 'HEADING',
                                placeholder: true
                            },
                            icon: 'fa-header'
                        },
                        {
                            title: 'Image',
                            action: 'ADD_IMAGE',
                            params: {
                                placeholder: true
                            },
                            icon: 'fa-image'
                        }
                    ]
                },
                {
                    title: 'Layouts',
                    tools: [
                        {
                            title: '1 Column',
                            action: 'ADD_COL',
                            params: {
                                num: 1
                            },
                            iconCol: 1
                        },
                        {
                            title: '2 Column',
                            action: 'ADD_COL',
                            params: {
                                num: 2
                            },
                            iconCol: 2
                        },
                        {
                            title: '3 Column',
                            action: 'ADD_COL',
                            params: {
                                num: 3
                            },
                            iconCol: 3
                        },
                        {
                            title: '4 Column',
                            action: 'ADD_COL',
                            params: {
                                num: 4
                            },
                            iconCol: 4
                        },
                        {
                            title: '5 Column',
                            action: 'ADD_COL',
                            params: {
                                num: 5
                            },
                            iconCol: 5
                        },
                        {
                            title: '6 Column',
                            action: 'ADD_COL',
                            params: {
                                num: 6
                            },
                            iconCol: 6
                        },
                        {
                            title: '8 Column',
                            action: 'ADD_COL',
                            params: {
                                num: 8
                            },
                            iconCol: 8
                        }
                    ]
                }
            ]
        }
    })

    .value('AssetsLibraryData', {
        data: [
            {
                id: 1,
                name: 'That title thing',
                content: 'Welcome to my world',
                size: 1,
                type: 'text',
                subtype: 'heading',
                modified: +new Date(),
                uploaded: +new Date()
            },
            {
                id: 2,
                name: 'food7.jpg',
                url: 'http://lorempixel.com/300/400/food/7/',
                size: 123,
                type: 'img',
                modified: +new Date(),
                uploaded: +new Date()
            },
            {
                id: 3,
                name: 'food8.jpg',
                url: 'http://lorempixel.com/300/400/food/8/',
                size: 5,
                type: 'img',
                modified: +new Date(),
                uploaded: +new Date()
            },
            {
                id: 4,
                name: 'Some text',
                content: 'Sea ad purto veniam animal. Ad eos iriure aliquando scripserit. Probo commodo nostrum cum ei, at ius simul convenire principes, dissentiunt necessitatibus ei cum. No utinam suavitate interpretaris ius. Cum saperet albucius intellegebat id.',
                size: 5,
                type: 'text',
                modified: +new Date(),
                uploaded: +new Date()
            },
            {
                id: 5,
                name: 'food9.jpg',
                url: 'http://lorempixel.com/300/400/food/9/',
                size: 22,
                type: 'img',
                modified: +new Date(),
                uploaded: +new Date()
            },
            {
                id: 6,
                name: 'food10.jpg',
                url: 'http://lorempixel.com/300/400/food/10/',
                size: 101,
                type: 'img',
                modified: +new Date(),
                uploaded: +new Date()
            },
            {
                id: 7,
                name: 'A titular entity',
                content: 'Won\'t you come on in',
                size: 1,
                type: 'text',
                subtype: 'heading',
                modified: +new Date(),
                uploaded: +new Date()
            },
            {
                id: 8,
                name: 'Lipsum3',
                content: 'Partem inciderint no vel, in cum eius nulla. Sit at aeterno commune repudiare, at usu offendit qualisque. No vim brute soleat luptatum. Ius an utamur expetenda. Consul inermis referrentur est te, ius velit quodsi eu, ad prima sensibus vulputate his. Sed natum omnesque te, at quando legendos nominati per, vim meis eirmod verear te.',
                size: 12,
                type: 'text',
                modified: +new Date(),
                uploaded: +new Date()
            }
        ]
    });
