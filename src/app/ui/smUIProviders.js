// Service providers for smUI module - providers, factories, services, values, constants

angular
    .module('smUI.providers', ['smUI.mockData'])

    .factory('UIModelService',
        [
            'UIModelDefault',
            function UIModelService(modelDefault) {

                var ModelService = {};

                ModelService.getSitesMenu = function getSitesMenu() {
                    return modelDefault.header.siteMenu;
                };

                ModelService.getDeviceTools = function getDeviceTools() {
                    return modelDefault.header.deviceTools;
                };

                ModelService.getPageTools = function getPageTools() {
                    return modelDefault.header.pageTools;
                };

                ModelService.getAppMenu = function getAppMenu() {
                    return modelDefault.header.appMenu;
                };

                ModelService.getToolbar = function getToolbar() {
                    return modelDefault.header.toolbar;
                };

                ModelService.getPanelDesign = function getPanelDesign() {
                    return modelDefault.panel.design;
                };

                return ModelService;
            }
        ]
    )

    .factory('UIAssetsService',
        [
            '$q', 'AssetsLibraryData',
            function UIAssetsService($q, assetsData) {

                var AssetsService = {};

                AssetsService.getAssets = function(userId) {

                    // Using promise because data will be fetch async using userId

                    var deferred = $q.defer();
                    deferred.resolve(assetsData);
                    return deferred.promise;
                };

                return AssetsService;
            }
        ]);
