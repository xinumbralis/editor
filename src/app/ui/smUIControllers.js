// Controllers for smUI module

angular
    .module('smUI.controllers', ['smUI.providers'])

    .controller('UIHeaderCtrl',
        [
            'UIModelService',
            function (UIModelService) {

                this.sites = UIModelService.getSitesMenu();
                this.devices = UIModelService.getDeviceTools();
                this.pageTools = UIModelService.getPageTools();
                this.appMenu = UIModelService.getAppMenu();
            }
        ]
    )

    .controller('UIToolbarCtrl',
        [
            'UIModelService',
            function (UIModelService) {

                this.tools = UIModelService.getToolbar();
            }
        ]
    )

    .controller('UIPanelDesignCtrl',
        [
            'UIModelService',
            function (UIModelService) {

                this.tools = UIModelService.getPanelDesign();
                this.times = function times(n) {
                    return _.times(n, _.identity);
                };
            }
        ]
    )

    .controller('UIAssetsCtrl',
        [
            'UIAssetsService',
            function (UIAssets) {

                var vm = this;

                vm.library = [];
                vm.gridView = true;
                vm.reverse = false;
                vm.ordering = 'type';
                vm.toggleView = function () {
                    vm.gridView = !vm.gridView;
                    return vm.gridView;
                };

                vm.order = function(type) {
                    vm.lastOrder = _.toArray(type).join('');
                    vm.reverse = vm.lastOrder === _.toArray(vm.ordering).join('') ? !vm.reverse : false;
                    vm.ordering = type;
                };

                UIAssets
                    .getAssets('abc123')
                    .then(function(assets) {
                        vm.library = assets.data;
                    });
            }
        ]
    )

    .controller('UIAssetsPreviewCtrl',
        [
            '$stateParams', '$state', 'library',
            function($stateParams, $state, library) {
                this.asset = _.first(_.filter(library,
                    {id: _.parseInt($stateParams.assetId)}));
            }
        ]
    );
