// UI module

angular
    .module(
        'smUI',
        ['ui.router', 'smUI.controllers', 'smUI.directives', 'ngAnimate']
    )

    .config([
        '$stateProvider', '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {

            // ui-router helper - user angular templateCache service to fetch pre-compiled templates.

            var loadTemplate = function (template) {
                return ['$templateCache', function ($templateCache) {
                    return $templateCache.get(template);
                }];
            };

            // App UI routing

            $urlRouterProvider.otherwise('/editor');   // TODO: Proper routing and otherwise for 404
 
            // App UI states

            $stateProvider
                .state('editor', {
                    url: '/editor',
                    views: {
                        'app': {
                            templateProvider: loadTemplate('/app/ui/views/app-editor.html')
                        },
                        'header': {
                            templateProvider: loadTemplate('/app/ui/views/header.html')
                        }
                    }
                })
                .state('editor.design', {
                    url: '/design',
                    templateProvider: loadTemplate('/app/ui/views/app-editor-design.html'),
                    controller: 'UIPanelDesignCtrl as panelDesign'
                })
                .state('editor.theme', {
                    url: '/theme',
                    templateProvider: loadTemplate('/app/ui/views/app-editor-theme.html')
                })
                .state('editor.assets', {
                    url: '/assets',
                    templateProvider: loadTemplate('/app/ui/views/app-editor-assets.html'),
                    controller: 'UIAssetsCtrl as assets'
                })
                .state('editor.assets.preview', {
                    url: '/:assetId',
                    views: {
                        '': {
                            templateProvider: loadTemplate('/app/ui/views/app-editor-assets-preview.html'),
                            controller: 'UIAssetsPreviewCtrl as preview',
                            resolve: {
                                library: ['UIAssetsService', 
                                    function(assetsService) {
                                        return assetsService.getAssets()
                                            .then(function(lib) {
                                                return lib.data;
                                            });
                                    }]
                            }
                        }
                    }
                });
        }
    ])

    .run([
        '$rootScope', '$state', '$stateParams',
        function ($rootScope, $state, $stateParams) {

            // Expose ui-router state to the angular root scope

            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;
        }
    ]);
